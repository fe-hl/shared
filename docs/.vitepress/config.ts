import { defineConfig } from 'vitepress';
import path from 'path';
const resolve = dir => path.join(__dirname, dir);
const IS_PROD = () => {
	return process.argv[process.argv.length - 1] === 'build';
};
export default defineConfig({
	outDir: resolve('../../document'),
	base: IS_PROD() ? '/shared/' : '/',
	title: '工具库文档',
	description: '工具库文档',
	head: [
		['link', { rel: 'icon', type: 'image/svg+xml', href: 'images/logo.svg' }],
	],
	lastUpdated: true,
	markdown: {
		lineNumbers: false,
		config: md => {
			const { demoBlockPlugin } = require('vitepress-theme-demoblock');
			md.use(demoBlockPlugin, {
				cssPreprocessor: 'scss',
			});
		},
	},
	themeConfig: {
		lastUpdatedText: '上次更新',
		editLink: {
			pattern: 'https://gitee.com/fe-hl/shared',
			text: '在GitHub上编辑此页',
		},
		logo: 'images/logo.svg',
		nav: [
			{ text: '主页', link: '/' },
			{ text: '指南', link: '/pages/guide' },
			{ text: '更新日志', link: '/pages/updateLog' },
		],

		socialLinks: [
			{
				icon: 'github',
				link: 'https://gitee.com/fe-hl/shared',
			},
		],
		sidebar: [
			{
				collapsed: false,
				items: [
					{
						text: '开发指南',
						link: '/pages/guide',
					},
				],
			},
			{
				collapsed: false,
				items: [
					{
						text: '日期处理',
						link: '/pages/date',
					},
				],
			},
			{
				collapsed: false,
				items: [
					{
						text: 'storage存储',
						link: '/pages/storage',
					},
				],
			},
			{
				collapsed: false,
				items: [
					{
						text: '正则',
						link: '/pages/regularExp',
					},
				],
			},
			{
				collapsed: false,
				items: [
					{
						text: '金额处理',
						link: '/pages/bigDecimal',
					},
				],
			},
			{
				collapsed: false,
				items: [
					{
						text: '工具函数',
						link: '/pages/utils',
					},
				],
			},
		],
	},
});
