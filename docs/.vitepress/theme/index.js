import DefaultTheme from 'vitepress/theme';

import 'vitepress-theme-demoblock/theme/styles/index.css';
import Vant from 'vant';
import Demo from 'vitepress-theme-demoblock/components/Demo.vue';
import DemoBlock from 'vitepress-theme-demoblock/components/DemoBlock.vue';
import 'vant/lib/index.css';
import './index.scss';
import * as shared from '@fe-hl/shared';
function setWindowKeys(obj) {
	try {
		Object.keys(obj).forEach(key => (window[key] = obj[key]));
	} catch (error) { }
}
export default {
	...DefaultTheme,
	enhanceApp({ app }) {
		// 注入Window
		setWindowKeys(shared);
		app.use(Vant);
		app.component('Demo', Demo);
		app.component('DemoBlock', DemoBlock);
		app.component('DemoBlock', DemoBlock);
	},
};
