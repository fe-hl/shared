# 日期处理

## 函数引用

```js
import {
	dateAddDay,
	dateAddMonth,
	dateAddYears,
	dateDiffDay,
	dateDiffHour,
	dateDiffMonth,
	dateDiffYears,
	dateFormat,
	dateSubtractDay,
	dateSubtractMonth,
	dateSubtractYears,
	dateToTime,
} from '@fe-hl/shared';
```

## 函数调用参数说明

```js
type FormatType = 'YYYY-MM-DD' | 'YYYY-MM-DD HH:mm:ss' | 'HH:mm:ss';
type DateType = Date | number;

dateFormat: (d?: DateType, format?: FormatType) => string;
dateToTime: (d: Date) => number;
dateAddDay: (n: number, d?: DateType, format?: FormatType) => string;
dateSubtractDay: (n: number, d?: DateType, format?: FormatType) => string;
dateAddMonth: (n: number, d?: DateType, format?: FormatType) => string;
dateSubtractMonth: (n: number, d?: DateType, format?: FormatType) => string;
dateAddYears: (n: number, d?: DateType, format?: FormatType) => string;
dateSubtractYears: (n: number, d?: DateType, format?: FormatType) => string;
dateDiffYears: (diffDate: DateType, d?: DateType) => number;
dateDiffMonth: (diffDate: DateType, d?: DateType) => number;
dateDiffDay: (diffDate: DateType, d?: DateType) => number;
dateDiffHour: (diffDate: DateType, d?: DateType) => number;
```

## 参数默认值

- d 默认值 new Date()
- format 默认值 YYYY-MM-DD

## dateFormat 日期格式化

```js
dateFormat(new Date(), 'YYYY-MM-DD'); // 2022-08-26
```

## dateToTime 日期格转时间戳

```js
dateToTime(new Date()); // 1661507946280
```

## dateAddDay 日期加几天

```js
dateAddDay(1, new Date(), 'YYYY-MM-DD'); // 2022-08-27
```

## dateSubtractDay 日期减几天

```js
dateSubtractDay(1, new Date(), 'YYYY-MM-DD'); // 2022-08-25
```

## dateAddMonth 日期加几个月

```js
dateAddMonth(1, new Date(), 'YYYY-MM-DD'); // 2022-09-26
```

## dateSubtractMonth 日期减几个月

```js
dateSubtractMonth(1, new Date(), 'YYYY-MM-DD'); // 2022-07-26
```

## dateAddYears 日期加几年

```js
dateAddYears(1, new Date(), 'YYYY-MM-DD'); // 2023-07-26
```

## dateSubtractYears 日期减几年

```js
dateSubtractYears(1, new Date(), 'YYYY-MM-DD'); // 2021-07-26
```

## dateDiffYears 日期差几年

```js
dateDiffYears(new Date('2022-08-26'), new Date('2023-08-26')); // 1
```

## dateDiffMonth 日期差几月

```js
dateDiffMonth(new Date('2022-08-26'), new Date('2022-09-26')); // 1
```

## dateDiffDay 日期差几天

```js
dateDiffDay(new Date('2022-08-26'), new Date('2022-08-27')); // 1
```

## dateDiffHour 日期差几小时

```js
dateDiffHour(new Date('2022-08-26 10:00:00'), new Date('2022-08-26 11:00:00')); // 1
```
