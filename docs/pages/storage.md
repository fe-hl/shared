# storage 存储

## 函数引用

```js
import { storageGet, storageSet, storageDeleteKey } from '@fe-hl/shared';
```

## 函数调用参数说明

```js
enum STORAGE {
    LOCAL = "localStorage",
    SESSION = "sessionStorage"
}
storageGet: <T extends object>(key: string, type?: STORAGE) => string | T;
storageSet: (key: string, value: any, type?: STORAGE) => void;
storageDeleteKey: (key: string, type?: STORAGE) => void;

```

## 参数默认值 `[默认存储方式 sessionStorage]`

- type 默认值 STORAGE.SESSION = sessionStorage

## sessionStorage 存储

```js
storageSet('USER_INFO', { name: 'fe-hl' });
```

## sessionStorage 取值

```js
storageGet('USER_INFO'); // { name: 'fe-hl' }
```

## localStorage 存储

```js
storageSet('USER_INFO', { name: 'fe-hl' }, STORAGE.LOCAL);
```

## localStorage 取值

```js
storageGet('USER_INFO', STORAGE.LOCAL); // { name: 'fe-hl' }
```

## sessionStorage 删除某个 key

```js
storageDeleteKey('USER_INFO');
```

## localStorage 删除某个 key

```js
storageDeleteKey('USER_INFO', STORAGE.LOCAL);
```
