# 金额处理

## 解决计算精度丢失问题

## 函数引用

```js
import {
	bigPlus,
	bigSubtract,
	bigMultiply,
	bigDivide,
	bigRound,
	bigFenToYuan,
	bigYuanToFen,
	bigYuanTowan,
} from '@fe-hl/shared';
```

## 函数调用参数说明

```js
type BigSource = number | string;
export declare const bigPlus: (...values: BigSource[]) => string;
export declare const bigSubtract: (...values: BigSource[]) => string;
export declare const bigMultiply: (...values: BigSource[]) => string;
export declare const bigDivide: (...values: BigSource[]) => string;
bigRound: (num: BigSource, digits?: number) => string;
bigFenToYuan: (fen: BigSource, digits?: number) => string;
bigYuanToFen: (yuan: BigSource, digits?: number) => string;
bigYuanTowan: (yuan: BigSource, digits?: number) => string;
```

## 参数默认值 `小数位`

- digits 默认值 2

## 加

```js
bigPlus(50,50); // 100
bigPlus(50,50,50); // 150
```

## 减

```js
bigSubtract(110,10); // 100
bigSubtract(110,10,50); // 50
```

## 乘

```js
bigMultiply(10,10); // 100
bigMultiply(10,10,10); // 1000
```

## 除

```js
bigDivide(1000,10); // 100
bigDivide(1000,10,10); // 10
```

## 四舍五入`默认保留2位小数`

```js
bigRound(10.889); // 10.89
```

## 分转元`默认保留2位小数`

```js
bigFenToYuan(10000); //100.00
```

## 元转分`默认保留2位小数`

```js
bigYuanToFen(1); //100.00
```

## 元转万`默认保留2位小数`

```js
bigYuanTowan(1000000); //100.00
```
