# 正则

## 函数引用

```js
import {
	isMobile,
	isSMs,
	isNumber,
	isCard,
	isEmail,
	isPhone,
	isURl,
	isPwd,
	isMoney,
	isChinese,
} from '@fe-hl/shared';
```

## 函数调用参数说明

```js
const patternMobile: RegExp;
const patternNumber: RegExp;
const patternCard: RegExp;
const patternEmail: RegExp;
const patternPhone: RegExp;
const patternURl: RegExp;
const isMobile: (value: string) => boolean;
const patternSMs: (len?: number) => RegExp;
const isSMs: (value: string, len?: number) => boolean;
const isNumber: (value: string) => boolean;
const isCard: (value: string) => boolean;
const isEmail: (value: string) => boolean;
const isPhone: (value: string) => boolean;
const isURl: (value: string) => boolean;
const patternPwd: (beginLen?: number, endLen?: number) => RegExp;
const isPwd: (value: string, beginLen?: number, endLen?: number) => boolean
const patternMoney: (beginLen?: number, endLen?: number) => RegExp;
const isMoney: (value: string, beginLen?: number, endLen?: number) => boolean;
const patternChinese: (beginLen?: number, endLen?: number) => RegExp;
const isChinese: (value: string, beginLen?: number, endLen?: number) => boolean;

```

## isMobile 验证手机号

```js
isMobile('17621959533'); // true
```

## isSMs 验证短信`默认为6位数字`

```js
isSMs('123456'); // true

isSMs('1234', 4); // true 四位
```

## isNumber 验证数字

```js
isNumber('123456'); // true
```

## isCard 验证身份证号码

```js
isCard('420881199411238221'); // true
```

## isEmail 验证邮箱

```js
isEmail('hu.lei@qq.com'); // true
```

## isPhone 验证座机`中间7到8位，最后一部分可不要或者1到4位`

```js
isPhone('021-12345678-1234'); // true
```

## isURl 验证 URl

```js
isURl('http://www.baidu.com'); // true
isURl('https://www.baidu.com'); // true
isURl('www.baidu.com'); // true
isURl('baidu.com'); // true
```

## isPwd 验证 密码`密码以字母开头，长度默认在6~18之间，只能包含字母、数字和下划线`

```js
isPwd('w12345ix'); // true
isPwd('w12345ix', 4, 8); // true 5到9位
```

## isMoney 验证小数位`默认1到2位`

```js
isMoney('1.98'); // true
isMoney('1.983', 1, 3); // true  1到3位
```

## isChinese 验证中文`默认1到2位`

```js
isChinese('张三'); // true
isChinese('张三三', 1, 3); // true 1到3位
```
