import Big, { BigSource } from 'big.js';
// 加
export const bigPlus = (...values: BigSource[]) => {
	return values
		.reduce((prev, cur) => {
			return new Big(prev).plus(new Big(cur));
		}, 0)
		.toString();
};
// 减
export const bigSubtract = (...values: BigSource[]) => {
	return values
		.reduce((prev, cur) => {
			return new Big(prev).minus(new Big(cur));
		})
		.toString();
};
// 乘
export const bigMultiply = (...values: BigSource[]) => {
	return values
		.reduce((prev, cur) => {
			return new Big(prev).times(new Big(cur));
		})
		.toString();
};
// 除
export const bigDivide = (...values: BigSource[]) => {
	return values
		.reduce((prev, cur) => {
			return new Big(prev).div(new Big(cur));
		})
		.toString();
};
// 四舍五入
export const bigRound = (num: BigSource, digits = 2) => {
	return new Big(num).round(digits).toString();
};

// 分转元
export const bigFenToYuan = (fen: BigSource, digits = 2) => {
	return Number(bigDivide(fen, 100)).toFixed(digits);
};
// 元转分
export const bigYuanToFen = (yuan: BigSource, digits = 2) => {
	return Number(bigMultiply(yuan, 100)).toFixed(digits);
};
// 元转万
export const bigYuanTowan = (yuan: BigSource, digits = 2) => {
	if (yuan) {
		return Number(bigDivide(yuan, 10000)).toFixed(digits);
	}
	if (digits > 0) {
		return '0.' + new Array(digits).fill(0).join('');
	}
	return '0';
};
